﻿using System;
using System.Collections.Generic;
using System.Text;
using RPGgame.Hero;

namespace RPGgame.UI
{
    public class UICharacter
    {
        public static void PrintCharacterBaseInfo(IHero hero)
        {

            StringBuilder sb = new StringBuilder(50);

            sb.Append($"\n{hero.ClassName} Details: \n");
            sb.Append($"\nYour Character: {hero.Name}");



            sb.Append("\nHP: " + hero.Hp.ToString());
            sb.Append("\nStr: " + hero.Str.ToString());
            sb.Append("\nDex: " + hero.Dex.ToString());
            sb.Append("\nInt: " + hero.Intelligence.ToString());
            sb.Append("\nLvl: " + hero.Lvl.ToString());
            sb.Append("\nXP to next: " + hero.NextLvlExp.ToString());

            string message = sb.ToString();
            Console.WriteLine(message);
        }
        public static void PrintCharacterInfo(IHero hero) {

            StringBuilder sb = new StringBuilder(50);

            sb.Append($"\n{hero.ClassName} Details: \n");
            sb.Append($"\nYour Character: {hero.Name}");



            sb.Append("\nHP: " + hero.EffHp.ToString());
            sb.Append("\nStr: " + hero.EffStr.ToString());
            sb.Append("\nDex: " + hero.EffDex.ToString());
            sb.Append("\nInt: " + hero.EffIntelligence.ToString());
            sb.Append("\nLvl: " + hero.Lvl.ToString());
            sb.Append("\nXP to next: " + hero.NextLvlExp.ToString());

            string message = sb.ToString();
            Console.WriteLine(message);
        }

        public static void PrintCharacterItemInfo(IHero hero)
        {

            StringBuilder sb = new StringBuilder(50);

            sb.Append($"\n{hero.ClassName} Details: \n");
            sb.Append($"\nYour Character: {hero.Name}");


            sb.Append("\nHead: " + hero.Head.Name);
            sb.Append("\nBody: " + hero.Body.Name);
            sb.Append("\nLegs: " + hero.Legs.Name);
            sb.Append("\nWeapon: " + hero.Weapon.Name);

            string message = sb.ToString();

            if (message == "")
            {
                message = "You have no items!";
            }
            Console.WriteLine(message);
        }

    }
}
