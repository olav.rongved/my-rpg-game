﻿using RPGgame.Hero;
using RPGgame.Item;
using RPGgame.Item.Weapons;
using RPGgame.World.Monster;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGgame.UI
{
    public class UI
    {
        public static void Start()
        {
            while (true)
            {
                string WelcomeStr = "Welcome to the RPG game!\n" +
                                    "Please create your character!: \n\n" +
                                    "1) Mage\n" +
                                    "2) Warrior\n" +
                                    "3) Ranger\n";

                Console.WriteLine(WelcomeStr);

                ConsoleKeyInfo UserInput = Console.ReadKey();

                int choice;
                int.TryParse(UserInput.KeyChar.ToString(), out choice);

                Console.WriteLine("\n\n");

                switch (choice)
                {
                    case 1:
                        Console.WriteLine("You chose 1");
                        Mage testMage = new Mage();
                        MainMenu(testMage);
                        break;
                    case 2:
                        Console.WriteLine("You chose 2");
                        Warrior testWarrior = new Warrior();
                        MainMenu(testWarrior);
                        break;
                    case 3:
                        Console.WriteLine("You chose 3");
                        Ranger testRanger = new Ranger();
                        MainMenu(testRanger);
                        break;
                    default:
                        Console.WriteLine("Please choose a class");
                        break;

                }

            }
        }

        public static void MainMenu(IHero hero)
        {
            while (true)
            {
                string WelcomeStr = "Welcome to the RPG game!\n" +
                                    "Please create your character!: \n\n" +
                                    "1) Display hero base stats!\n" +
                                    "2) Display hero stats with items!\n" +
                                    "3) Display current items!\n" +
                                    "4) Equip a new item!\n" +
                                    "5) Create a new hero!\n" +
                                    "6) Attack the angry monster!\n" + 
                                    "7) Lvl up!\n";

                Console.WriteLine(WelcomeStr);

                ConsoleKeyInfo UserInput = Console.ReadKey();

                int choice;
                int.TryParse(UserInput.KeyChar.ToString(), out choice);

                Console.WriteLine("\n\n");

                switch (choice)
                {
                    case 1:
                        UICharacter.PrintCharacterInfo(hero);
                        break;
                    case 2:
                        UICharacter.PrintCharacterBaseInfo(hero);
                        break;
                    case 3:
                        UICharacter.PrintCharacterItemInfo(hero);
                        break;
                    case 4:
                        ItemSelect(hero);
                        break;
                    case 5:
                        Start();
                        break;
                    case 6:
                        Monster enemy = new Monster();
                        hero.Attack(enemy);
                        break;
                    case 7:
                        hero.LvlUp();
                        break;
                    default:
                        Console.WriteLine("Please choose a class");
                        break;

                }

            }
        }


        public static void ItemSelect(IHero hero)
        {

            string WelcomeStr =
                                "Please choose!: \n\n" +
                                "1) Equip new Head\n" +
                                "2) Equip new Body\n" +
                                "3) Equip new Legs\n" +
                                "4) Equip new Weapon\n" +
                                "5) Go back to main menu\n";

            Console.WriteLine(WelcomeStr);

            ConsoleKeyInfo UserInput = Console.ReadKey();

            int choice;
            int.TryParse(UserInput.KeyChar.ToString(), out choice);

            Console.WriteLine("\n\n");

            switch (choice)
            {
                case 1:
                    ArmorSelect(hero, "head");
                    //MainMenu(hero);
                    break;
                case 2:
                    ArmorSelect(hero, "body");
                    //MainMenu(hero);
                    break;
                case 3:
                    ArmorSelect(hero, "legs");
                    //MainMenu(hero);
                    break;
                case 4:
                    WeaponSelect(hero);
                    MainMenu(hero);
                    break;
                case 5:
                    MainMenu(hero);
                    break;
                default:
                    MainMenu(hero);
                    break;

            }

        }



        public static void WeaponSelect(IHero hero)
        {

            string WelcomeStr =
                                $"Please choose what type you need {hero.Name}!: \n\n" +
                                "1) Ranged!\n" +
                                "2) Melee!\n" +
                                "3) Magic!\n";

            Console.WriteLine(WelcomeStr);

            ConsoleKeyInfo UserInput = Console.ReadKey();

            int choice;
            int.TryParse(UserInput.KeyChar.ToString(), out choice);

            Console.WriteLine("\n\n");

            switch (choice)
            {
                case 1:
                    RangedWeapon rangedWeapon = new RangedWeapon("Bow of truth", "ranged", 99);
                    hero.EquipWeapon(rangedWeapon);
                    UIEquipment.PrintWeaponInfo(rangedWeapon);
                    MainMenu(hero);
                    break;
                case 2:
                    MeleeWeapon meleeWeapon = new MeleeWeapon("Hammer of truth", "melee", 99);
                    hero.EquipWeapon(meleeWeapon);
                    UIEquipment.PrintWeaponInfo(meleeWeapon);
                    MainMenu(hero);
                    break;
                case 3:
                    MagicWeapon magicWeapon = new MagicWeapon("Stick of truth", "magic", 99);
                    hero.EquipWeapon(magicWeapon);
                    UIEquipment.PrintWeaponInfo(magicWeapon);
                    MainMenu(hero);
                    break;
                default:
                    Console.WriteLine("Please choose a class");
                    break;

            }

        }
        public static void ArmorSelect(IHero hero, string slot)
        {

            string WelcomeStr =
                                $"Please choose what type you need {hero.Name}!: \n\n" +
                                "1) Cloth!\n" +
                                "2) Plate!\n" +
                                "3) Leather!\n";

            Console.WriteLine(WelcomeStr);

            ConsoleKeyInfo UserInput = Console.ReadKey();

            int choice;
            int.TryParse(UserInput.KeyChar.ToString(), out choice);

            Console.WriteLine("\n\n");

            switch (choice)
            {
                case 1:
                    Armor clothArmor = new Armor($"{slot} of Lord {hero.Name}", "cloth", $"{slot}", 2);
                    hero.EquipArmor(clothArmor);
                    UIEquipment.PrintArmorInfo(clothArmor);
                    MainMenu(hero);
                    break;
                case 2:
                    Armor plateArmor = new Armor($"{slot} of Lord {hero.Name}", "cloth", $"{slot}", 2);
                    UIEquipment.PrintArmorInfo(plateArmor);
                    MainMenu(hero);
                    break;
                case 3:
                    Armor LeatherArmor = new Armor($"{slot} of Lord {hero.Name}", "cloth", $"{slot}", 2);
                    UIEquipment.PrintArmorInfo(LeatherArmor);
                    MainMenu(hero);
                    break;
                default:
                    Console.WriteLine("Please choose a class");
                    break;

            }

        }

    }
}
