﻿using RPGgame.Item;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGgame.UI
{
    public class UIEquipment
    {

        public static void PrintWeaponInfo(IWeapon weapon)
        {
            StringBuilder sb = new StringBuilder(50);

            sb.Append("\n\nWeapon details: ");

            sb.Append("\n\nName: " + weapon.Name);
            sb.Append("\nWeapon Type: " + weapon.Type);
            sb.Append("\nWeapon Lvl: " + weapon.Lvl.ToString());
            sb.Append("\nWeapon Damage: " + weapon.Damage.ToString() + "\n");

            string message = sb.ToString();
            Console.WriteLine(message);
        }

        public static void PrintArmorInfo(IArmor armor)
        {
            StringBuilder sb = new StringBuilder(50);

            sb.Append("\n\nArmor details: ");

            sb.Append("\n\nName: " + armor.Name);
            sb.Append("\nArmor Slot: " + armor.Slot);

            sb.Append("\nArmor Type: " + armor.Type);
            sb.Append("\nArmor Damage: " + armor.Lvl + "\n");

            //TODO : get bonus stats

            string message = sb.ToString();
            Console.WriteLine(message);
        }
    }
}
