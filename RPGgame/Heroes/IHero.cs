﻿using RPGgame.Item;
using RPGgame.World.Monster;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGgame.Hero
{
    public interface IHero
    {
        string Name { get; set; }
        string ClassName { get; set; }
        double Exp { get; set; }
        double NextLvlExp { get; set; }
        int Lvl { get; set; }
        int Hp { get; set; }
        int Str { get; set; }
        int Intelligence { get; set; }
        int Dex { get; set; }

        int EffHp { get; set; }
        int EffStr { get; set; }
        int EffIntelligence { get; set; }
        int EffDex { get; set; }

        IArmor Head { get; set; }
        IArmor Body { get; set; }
        IArmor Legs { get; set; }

        IWeapon Weapon { get; set; }
        void UpdateNextLvlExp();
        void Attack(Monster monster);
        void ComputeEffectiveStats();

        void EquipWeapon(IWeapon NewWeapon);

        void EquipArmor(IArmor armor);

        void LvlUp();
    }
}
