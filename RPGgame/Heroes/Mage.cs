﻿using RPGgame.Item;
using RPGgame.Item.Weapons;
using RPGgame.World.Monster;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGgame.Hero
{
    public class Mage : IHero
    {
        public double Exp { get; set; }
        public double NextLvlExp { get; set; }
        public int Lvl { get; set; }
        public int Hp { get; set; }
        public int Str { get; set; }
        public int Intelligence { get; set; }
        public int Dex { get; set; }

        public IArmor Head { get; set; }
        public IArmor Body { get; set; }
        public IArmor Legs { get; set; }
        public IWeapon Weapon { get; set; }

        public int EffHp { get; set; }
        public int EffStr { get; set; }
        public int EffIntelligence { get; set; }
        public int EffDex { get; set; }

        public string Name { get; set; }
        public string ClassName { get; set; }

        public Mage()
        {
            NextLvlExp = 100;
            Exp = 0;
            Lvl = 1;

            // Base stats specific to class
            Hp = 100;
            Str = 2;
            Dex = 3;
            Intelligence = 10;

            EffHp = Hp;
            EffStr = Str;
            EffDex = Dex;
            EffIntelligence = Intelligence;

            // Dummy user
            Name = "Gandalf";
            ClassName = "Mage";

            Head = new Armor("Hat of Dewald", "cloth", "head", 38);
            Body = new Armor("Body of Dewald", "cloth", "body", 38);
            Legs = new Armor("Legs of Dewald", "cloth", "Legs", 38);
            Weapon = new MagicWeapon();
            ComputeEffectiveStats();


        }

        public void LvlUp()
        {
            Hp += 15;
            Str += 2;
            Dex += 3;
            Intelligence += 5;

            Lvl += 1;
            UpdateNextLvlExp();
        }

        public void ComputeEffectiveStats()
        {

            // For head
            int HeadBonusHP = Convert.ToInt32(Math.Round((Convert.ToDouble(Head.BonusHp) * (Convert.ToDouble(Head.Lvl)) * 5) * 0.8));
            int HeadBonusDex = Convert.ToInt32(Math.Round((Convert.ToDouble(Head.BonusDex) * (Convert.ToDouble(Head.Lvl)) * 1) * 0.8));
            int HeadBonusIntelligence = Convert.ToInt32(Math.Round(Convert.ToDouble((Head.BonusIntelligence) * (Convert.ToDouble(Head.Lvl)) * 2) * 0.8));
            int HeadBonusStr = Convert.ToInt32(Math.Round(Convert.ToDouble(Head.BonusStr) * (Convert.ToDouble(Head.Lvl)) * 0.8));
            // For Body

            int BodyBonusHP = Convert.ToInt32(Math.Round(Convert.ToDouble(Head.BonusHp) * (Convert.ToDouble(Head.Lvl)) * 1));
            int BodyBonusDex = Convert.ToInt32(Math.Round(Convert.ToDouble(Head.BonusDex) * (Convert.ToDouble(Head.Lvl)) * 1));
            int BodyBonusIntelligence = Convert.ToInt32(Math.Round(Convert.ToDouble(Head.BonusIntelligence) * (Convert.ToDouble(Head.Lvl)) * 1));
            int BodyBonusStr = Convert.ToInt32(Math.Round(Convert.ToDouble(Head.BonusStr) * (Convert.ToDouble(Head.Lvl)) * 1));

            // For Legs
            int LegsBonusHP = Convert.ToInt32(Math.Round(Convert.ToDouble(Head.BonusHp) * (Convert.ToDouble(Head.Lvl)) * 0.6));
            int LegsBonusDex = Convert.ToInt32(Math.Round(Convert.ToDouble(Head.BonusDex) * (Convert.ToDouble(Head.Lvl)) * 0.6));
            int LegsBonusIntelligence = Convert.ToInt32(Math.Round(Convert.ToDouble(Head.BonusIntelligence) * (Convert.ToDouble(Head.Lvl)) * 0.6));
            int LegsBonusStr = Convert.ToInt32(Math.Round(Convert.ToDouble(Head.BonusStr) * (Convert.ToDouble(Head.Lvl)) * 0.6));

            EffHp = Hp + (HeadBonusHP + BodyBonusHP + LegsBonusHP);
            EffDex = Dex + (HeadBonusDex + BodyBonusDex + LegsBonusDex);
            EffIntelligence = Intelligence + (HeadBonusIntelligence + BodyBonusIntelligence + LegsBonusIntelligence);
            EffStr = Str + (HeadBonusStr + BodyBonusStr + LegsBonusStr);

        }

        public void UpdateNextLvlExp()
        {
            NextLvlExp = NextLvlExp + Math.Round(NextLvlExp * 0.1);
        }

        public void ShowItems()
        {
            throw new NotImplementedException();
        }

        public void Attack(Monster monster)
        {
            int dps = Weapon.Damage + 3 * Intelligence;
            monster.Hp -= dps;
            Console.WriteLine($"You hit monster for {dps} dmg!");
            if (monster.Hp < 0)
            {
                Console.WriteLine($"Monster died!");
                GetExp(monster.Exp);
            }
        }

        public void GetExp(double exp)
        {
            if (exp > NextLvlExp) {
                Exp = 0;
                LvlUp();
            } else {
                Exp = exp + Exp;
            }
        }

        public void EquipWeapon(IWeapon NewWeapon)

        {
            Weapon = NewWeapon;
            ComputeEffectiveStats();
        }

        public void EquipArmor(IArmor armor)

        {
            switch (armor.Slot)
            {
                case "head":
                    Head = armor;
                    break;
                case "body":
                    Body = armor;
                    break;
                case "legs":
                    Legs = armor;
                    break;
                default:
                    Console.WriteLine("Error! Could not equip armor");
                    break;
            }
            ComputeEffectiveStats();
        }

    }
}
