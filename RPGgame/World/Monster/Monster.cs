﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGgame.World.Monster
{
    public class Monster
    {
        public int Hp { get; set; }
        public double Exp { get; set; }

        public Monster()
        {
            Hp = 100;
            Exp = 50;
        }

    }
}
