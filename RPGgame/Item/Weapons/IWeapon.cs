﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGgame.Item
{
    public interface IWeapon
    {
        string Name { get; set; }
        string Type { get; set; }
        int Damage { get; set; }
        int Lvl { get; set; }
        int BonusHp { get; set; }
        int BonusStr { get; set; }
        int BonusDex { get; set; }
        int BonusIntelligence { get; set; }

        
    }
}
