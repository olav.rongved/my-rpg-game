﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGgame.Item.Weapons
{
    class MagicWeapon : IWeapon
    {
        public string Name { get; set; }
        public int Damage { get; set; }
        public string Type { get; set; }
        public int Lvl { get; set; }
        public int BonusHp { get; set; }
        public int BonusStr { get; set; }
        public int BonusDex { get; set; }
        public int BonusIntelligence { get; set; }

        public MagicWeapon()
        {
            Name = "Starter Weapon";
            Damage = 25;
            Type = "magic";
            Lvl = 1;

        }


        // Override
        public MagicWeapon(string name, string type, int lvl)
        {
            Name = name;
            Damage = 25 + lvl*2;
            Type = type;
            Lvl = lvl;

        }
    }
}
