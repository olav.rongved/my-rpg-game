﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGgame.Item.Weapons
{
    public class MeleeWeapon : IWeapon
    {
        public string Name { get; set; }
        public int Damage { get; set; }
        public string Type { get; set; }
        public int Lvl { get; set; }
        public int BonusHp { get; set; }
        public int BonusStr { get; set; }
        public int BonusDex { get; set; }
        public int BonusIntelligence { get; set; }

        public MeleeWeapon()
        {
            Name = "Starter Weapon";
            Damage = 15;
            Type = "magic";
            Lvl = 1;

        }
        public MeleeWeapon(string name,  string type, int lvl)
        {
            Name = name;
            Damage = 15 + lvl*2;
            Type = type;
            Lvl = lvl;

        }
    }
}
