﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGgame.Item
{
    public class Armor : IArmor
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Slot { get; set; }
        public int Lvl { get; set; }
        public int BonusHp { get; set; }
        public int BonusStr { get; set; }
        public int BonusDex { get; set; }
        public int BonusIntelligence { get; set; }

        public Armor()
        {
            Name = "SuperGear";
            Type = "cloth";
            Slot = "head";

            Lvl = 1;
            BonusHp = 10;
            BonusIntelligence = 3;
            BonusDex = 1;
            BonusStr = 0;

        }


        public Armor(string name, string type, string slot, int lvl)
        {
            // default 
            Name = name;
            Type = type;
            Slot = slot;
            Lvl = lvl;

            BonusHp = 10;
            BonusIntelligence = 3;
            BonusDex = 1;
            BonusStr = 0;

        }

        public Armor(string name, string type, string slot, int lvl,
                     int bHp, int bInt,int bDex,int bStr)
        {
            Name = "SuperGear";
            Type = "cloth";
            Slot = "head";

            Lvl = 1;
            BonusHp = 10;
            BonusIntelligence = 3;
            BonusDex = 1;
            BonusStr = 0;

        }

        // TODO : if type = cloth,leather, plate ...
    }
}
