﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPGgame.Item
{
    public interface IArmor
    {
        string Name { get; set; }
        string Type { get; set; }
        string Slot { get; set; }
        int Lvl { get; set; }
        int BonusHp { get; set; }
        int BonusStr { get; set; }
        int BonusDex { get; set; }
        int BonusIntelligence { get; set; }

    }
}
